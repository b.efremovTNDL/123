//--slider--//

const container = document.querySelector(".container")
const swiper = new Swiper('.hero__swiper', {
  slidesPerView: 1,
  loop: true,
  speed: 300,
  pagination: {
    el: '.hero__pagination',
    type: 'bullets',
    clickable: true
  },
  })

//--accordion--//

 $ (".accordion").accordion ({
  heighStyle: "content",
  active: 5
 });

 //--tabs--//

 document.querySelectorAll('.tabs-nav__btn').forEach(function(tabsBtn){
  tabsBtn.addEventListener('click', function(e){
  const path = e.currentTarget.dataset.path;

  document.querySelectorAll('.tabs-nav__btn').forEach(function(btn){
  btn.classList.remove('tabs-nav__btn--active')});
  e.currentTarget.classList.add('tabs-nav__btn--active');
    document.querySelectorAll('.tabs-item').forEach(function(tabsBtn){
  tabsBtn.classList.remove('tabs-item--active')});

  document.querySelector(`[data-target="${path}"]`).classList.add('tabs-item--active');
  });
    });

    //--burger--//

    let burger = document.querySelector('.header__burger-button');
    let menu = document.querySelector('.header__nav')

    burger.addEventListener('click',

    function() {
      burger.classList.toggle('header__burger-button--active');
      menu.classList.toggle('header__nav--active');

      document.body.classList.toggle('stop-scroll');
    });

    menuLinks.forEach(function(el){
      el.addEventListener('clock', function() {

        burger.classList.remove('header__burger-button--active');
        menu.classList.remove('header__nav--active');
        document.body.classList.remove('stop-scroll');
      })
    });
